<?php 


//$api_key = $_GET['api_key'];
$api_key = "8374ad69afc27cf6bd09a06aa5270efc61464d2e";
$url = "http://mysupport.mojohelpdesk.com/api/tickets.json?access_key=" . $api_key;

if(isset($api_key)) {	

	$data_json = getData($url);
	
	include('view.php');	

}
else {

	echo "wala";
}


function getData($url) {
	//  Initiate curl
	$ch = curl_init();
	// Disable SSL verification
	curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
	// Will return the response, if false it print the response
	curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
	// Set the url
	curl_setopt($ch, CURLOPT_URL,$url);
	// Execute
	$result=curl_exec($ch);
	// Closing
	curl_close($ch);

	// Will dump a beauty json :3
	return json_decode($result, true);
}


function getStatus($status) {	

	switch ($status) {
	    case "10":
	        return "New";
	        break;
	    case "20":
	        return "In Progress";
	        break;
	    case "30":
	        return "On Hold";
	        break;
	    case "40":
	        return "Information Requested";
	        break;
	    case "50":
	        return "Solved";
	        break;    
	    case "60":
	        return "Closed";
	        break;        
	    default:
	        echo "No status Available";
	}
}

function printData($string) {

	echo "<pre>" . print_r($string,1) . "</pre>";

}

function getName($id) {

	$array = array(60428 => 'Dev > General > [Innovid AdOps]', 63406 => 'QA > PP > [Innovid AdOps]', 94296 => 'Dev>General>[IAO Traffick]', 94297 => 'Dev>General>[IAO QA Revision]');

	return $array[$id];
}


function getUser($id, $api_key) {
	$user_url = "http://mysupport.mojohelpdesk.com/api/users/" . $id . ".json?access_key=" . $api_key;

	$user_data = getData($user_url);

	foreach ($user_data as $user) {
		return $user['first_name'] . " " . $user['last_name'];
	}

}

function getDateOnly($datetime) {
	
	$dt = new DateTime($datetime);

	$date = $dt->format('m/d/Y');	

	return $date;

	
}

function getTimeOnly($datetime) {
	
	$dt = new DateTime($datetime);

	
	$time = $dt->format('H:i:s');

	return $time;

	
}

/*function formatDate($date) {
	$raw = new DateTime($date);
	return $date->setTimeZone(new DateTimeZone("Asia/Manila");
}*/


?>