<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title>Mojo Helpdesk Api</title>
<script type="text/javascript" src="jquery-3.0.0.min.js"></script>
<script type="text/javascript" src="jquery.dataTables.min.js"></script>
<link rel="stylesheet" href="jquery.dataTables.min.css" type="text/css" media="all">

<style>

* {
	font-family: Arial, "Helvetica Neue", Helvetica, sans-serif;

}

td {
	text-align: center;
}

</style>

</head>

<body>
<div class="pageloader">Loading....</div>


	<div id="refresher">
	<h1 style="text-align: center;">Mojo Helpdesk</h1>
	<div id="table-holder">
	<table id="mojotable" class="display" cellspacing="0" width="100%">
        <thead>
            <tr>
                <th>Name</th>
                <th>Mojo ID</th>
                <th>Due Date</th>
                <th>Due Hour</th>
                <th>Status</th>
                <th>Queue Name</th>
                
            </tr>
        </thead>
        <tfoot>
            <tr>
                <th>Name</th>
                <th>Mojo ID</th>
                <th>Due Date</th>
                <th>Due Hour</th>
                <th>Status</th>
                <th>Queue Name</th>
                
            </tr>
        </tfoot>
        <tbody>
            <?php 
            	//printData($data_json);
				foreach($data_json as $data) {
					if($data['ticket']['user_id'] != 1501474) {
						if(($data['ticket']['ticket_queue_id'] == 60428) || 
							($data['ticket']['ticket_queue_id'] == 63406) ||
							($data['ticket']['ticket_queue_id'] == 94296) ||
							($data['ticket']['ticket_queue_id'] == 94297)
						
							) 


						{
									
						?>
						<tr>
							<td><?php echo getUser($data['ticket']['assigned_to_id'], $api_key);?></td>
							<td><?php echo $data['ticket']['id'];?></td>
							<td><?php echo getDateOnly($data['ticket']['scheduled_on']);?></td>
							<td><?php echo $data['ticket']['custom_field_commit_hour'];?></td>
							<td><?php echo getStatus($data['ticket']['status_id']);?></td>
							<td><?php 							
								echo getName($data['ticket']['ticket_queue_id']);

								?></td>
						</tr>

						<?php	
						}
					}
						
				}

			?>     
        </tbody>
    </table>
	</div>
	</div>


</body>

<script type="text/javascript">
$(document).ready(function() {

	$('.pageloader').hide();
    $('#mojotable').DataTable();



    setInterval(function()
	{ 	  
		location.reload();  
	    //$('#refresher').load('#refresher');	    
	}, 120000);



} );
</script>



</html>

